﻿using System;
namespace List
{
    public class DoubleLinkedList : IList
    {
        ListNode _head; 
        ListNode _tail; 
        int _size;
      
        public void AddFirst(int data)
        {
            ListNode node = new ListNode(data);
            ListNode temp = _head;
            node.Next = temp;
            _head = node;
            if (_size == 0)
                _tail = _head;
            else
                temp.Previous = node;
            _size++;
        }
        private void Add(int data)
        {
            ListNode node = new ListNode(data);

            if (_head == null)
                _head = node;
            else
            {
                _tail.Next = node;
                node.Previous = _tail;
            }
           _tail = node;
            _size++;
        }
        private bool Remove(int data)
        {
            ListNode current = _head;

            // поиск удаляемого узла
            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    break;
                }
                current = current.Next;
            }
            if (current != null)
            {
                // если узел не последний
                if (current.Next != null)
                {
                    current.Next.Previous = current.Previous;
                }
                else
                {
                    // если последний, переустанавливаем tail
                    _tail = current.Previous;
                }

                // если узел не первый
                if (current.Previous != null)
                {
                    current.Previous.Next = current.Next;
                }
                else
                {
                    // если первый, переустанавливаем head
                    _head = current.Next;
                }
                _size--;
                return true;
            }
            return false;
        }

        public bool IsEmpty { get { return _size == 0; } }

        //IEnumerator<T> IEnumerable<T>.GetEnumerator()
        //{
        //    ListNode<T> current = head;
        //    while (current != null)
        //    {
        //        yield return current.Data;
        //        current = current.Next;
        //    }
        //}

        //public IEnumerable<T> BackEnumerator()
        //{
        //    ListNode<T> current = tail;
        //    while (current != null)
        //    {
        //        yield return current.Data;
        //        current = current.Previous;
        //    }
        //}


        public void AddFirst(int[] vals)
        {
            for (int i = 0; i<vals.Length; i++)
            {
                AddFirst(vals[i]);
            }
        }

        public void AddLast(int val)
        {
            ListNode node = new ListNode(val);

            if (_size == 0)
            {
                _head = node;
            }
            else
            {
                _tail.Next = node;
                node.Previous = _tail;
            }

            _tail = node;
            _size++;
        }

        public void AddLast(int[] vals)
        {
            for (int i = 0; i < vals.Length; i++)
            {
                AddLast(vals[i]);
            }
        }

        public void  AddAt(int idx, int val)
        {
            if ((idx > _size) || (idx < 0)) throw new IndexOutOfRangeException();
            ListNode newNode = new ListNode(val);

            ListNode current = _head;
           
            int currentIdx = 0;
   
                while (current != null)
                {
                    if (currentIdx == idx)
                    {                     
                        if (current.Previous == null)
                        {
                            AddFirst(val);
                            return;
                        }
                        if (current.Next == null)
                        {
                            AddLast(val);
                            return;
                    }
                        newNode.Next = current;
                        current.Previous.Next = newNode;
                        newNode.Previous = current.Previous;
                        if (newNode.Next != null)
                        {
                            newNode.Next.Previous = newNode;
                        }

                    //newNode.Next = current;
                    //newNode.Previous = current.Previous;

                    //current.Previous.Next = newNode;
                    //current.Previous = newNode;
                    _size++;
                    return;
                }

                    //if (currentIdx == 0)
                    //{
                    //    AddFirst(val);
                    //}

                    //if (currentIdx == _size)
                    //{
                    //    AddLast(val);
                    //}

                currentIdx++;
                    current = current.Next;

                }
            
        }

        public void AddAt(int idx, int[] vals)
        {
            for (int i = 0; i < vals.Length; i++)
            {
               AddAt(idx+i,vals[i]); 
            }
        }

        public int GetSize()
        {
            return _size;
        }

        public void Set(int idx, int val)
        {
            if ((idx > _size) || (idx < 0)) throw new IndexOutOfRangeException();
            ListNode current = _head;
            int currentIdx = 0;
            while (current != null)
            {
                if (currentIdx == idx) {
                    current.Data=val; return;
                }
                current = current.Next;
                currentIdx++;
            }          
        }

        public void RemoveFirst()
        {
            if (_size != 0)
            {            
                _head = _head.Next;
                _size--;

                if (_size == 0)
                {
                    _tail = null;
                }
                else
                {                  
                    _head.Previous = null;
                }
            }
        }
    

        public void RemoveLast()
        {
            if (_size != 0)
            {
                if (_size == 1)
                {
                    _head = null;
                    _tail = null;
                }
                else
                {                  
                    _tail.Previous.Next = null;
                    _tail = _tail.Previous;
                }

                _size--;
            }
        }

        public void RemoveAt(int idx)
        {
            if ((idx > _size) || (idx < 0)) throw new IndexOutOfRangeException();
            ListNode current = _head;
            int currentIdx = 0;
            while (current != null)
            {
                if (currentIdx == idx) {
                    Remove(current.Data);
                }
                current = current.Next;
                currentIdx++;
            }       
        }

        public void RemoveAll(int val)
        {
            ListNode current = _head;
            while (current != null)
            {
                if (current.Data.Equals(val))
                    Remove(val);
                current = current.Next;
            }          
        }

        public bool Contains(int val)
        {
            ListNode current = _head;
            while (current != null)
            {
                if (current.Data.Equals(val))
                    return true;
                current = current.Next;
            }
            return false;
        }

        public int IndexOf(int val)
        {           
            ListNode current = _head;
            int currentIdx = 0;
            while (current != null)
            {
                if (current.Data == val) return currentIdx;
                current = current.Next;
                currentIdx++;
            }
            return -1;
        }

        public int[] ToArray()
        {
            int[] outputArray = new int[_size];

            ListNode current = _head;
            int currentIdx = 0;
            while (current != null)
            {
                outputArray[currentIdx] = current.Data;
                current = current.Next;               
                currentIdx++;
            }
            return outputArray;
        }

        public int GetFirst()
        {
            if (_head!= null) { return _head.Data; }
            throw new NullReferenceException("First element can't be null");
        }

        public int GetLast()
        {
            if (_tail != null) { return _tail.Data; }
            throw new NullReferenceException("Last element can't be null");
        }

        public int Get(int idx)
        {
           if ((idx>_size) || (idx<0)) throw new IndexOutOfRangeException();        
            ListNode current = _head;
            int currentIdx = 0;
            while (current != null)
            {
                if (currentIdx == idx) return current.Data;
                current = current.Next;
                currentIdx++;
            }
         throw new NullReferenceException();
        }

        public void Reverse()
        {
            ListNode prev = null, 
                current = _head, next = null;
            while (current != null)
            {
                next = current.Next;
                current.Next = prev;
                prev = current;
                current = next;
            }
            _head = prev;
        }
    }
}
