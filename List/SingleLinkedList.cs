﻿using System;
namespace List
{
    public class SingleLinkedList: IList
    {
        public SingleLinkedList()
        {
        }

        public void AddAt(int idx, int val)
        {
            throw new NotImplementedException();
        }

        public void AddAt(int idx, int[] vals)
        {
            throw new NotImplementedException();
        }

        public void AddFirst(int val)
        {
            throw new NotImplementedException();
        }

        public void AddFirst(int[] vals)
        {
            throw new NotImplementedException();
        }

        public void AddLast(int val)
        {
            throw new NotImplementedException();
        }

        public void AddLast(int[] vals)
        {
            throw new NotImplementedException();
        }

        public bool Contains(int val)
        {
            throw new NotImplementedException();
        }

        public int Get(int idx)
        {
            throw new NotImplementedException();
        }

        public int GetFirst()
        {
            throw new NotImplementedException();
        }

        public int GetLast()
        {
            throw new NotImplementedException();
        }

        public int GetSize()
        {
            throw new NotImplementedException();
        }

        public int IndexOf(int val)
        {
            throw new NotImplementedException();
        }

        public void RemoveAll(int val)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int idx)
        {
            throw new NotImplementedException();
        }

        public void RemoveFirst()
        {
            throw new NotImplementedException();
        }

        public void RemoveLast()
        {
            throw new NotImplementedException();
        }

        public void Reverse()
        {
            throw new NotImplementedException();
        }

        public void Set(int idx, int val)
        {
            throw new NotImplementedException();
        }

        public int[] ToArray()
        {
            throw new NotImplementedException();
        }
    }
}
