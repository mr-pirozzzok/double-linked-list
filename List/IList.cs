﻿using System;
namespace List
{
    public interface IList
    {
        //AddFirst(int val) - добавление в начало списка
        void AddFirst(int val);
        //AddFirst(int[] vals) - то же самое, но с массивом
        void AddFirst(int[] vals);
        //AddLast(int val) - добавление в конец списка
        void AddLast(int val);
        //AddLast(int[] vals) - то же самое, но с массивом
        void AddLast(int[] vals);
        //AddAt(int idx, int val) - вставка по указанному индексу
        void AddAt(int idx, int val);
        //AddAt(int idx, int[] vals) - то же самое, но с массивом
        void AddAt(int idx, int[] vals);
        //GetSize() - узнать кол-во элементов в списке
        int GetSize();
        //Set(int idx, int val) - поменять значение элемента с указанным индексом
        void Set(int idx, int val);
        //RemoveFirst() - удаление первого элемента
        void RemoveFirst();
        //RemoveLast() - удаление последнего элемента
        void RemoveLast();
        //RemoveAt(int idx) - удаление по индексу
        void RemoveAt(int idx);
        //RemoveAll(int val) - удалить все элементы, равные val
        void RemoveAll(int val);
        //Contains(int val) - проверка, есть ли элемент в списке
        bool Contains(int val);
        //IndexOf(int val) - вернёт индекс первого найденного элемента, равного val(или -1, если элементов с таким значением в списке нет)
        int IndexOf(int val);
        //ToArray() - преобразовать список в массив
        int[] ToArray();
        //GetFirst() - вернёт значение первого элемента списка
        int GetFirst();
        //GetLast() - вернёт значение последнего элемента списка
        int GetLast();
        //Get(int idx) - вернёт значение элемента списка c указанным индексом
        int Get(int idx);
        //Reverse() - изменение порядка элементов списка на обратный
        void Reverse();

    }





















}
