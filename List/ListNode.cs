﻿namespace List
{
    public class ListNode
    {
        public ListNode(int data)
        {
            Data = data;
        }
        public int Data { get; set; }
        public ListNode Previous { get; set; }
        public ListNode Next { get; set; }
    }
}
