﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Linq;

namespace List.Tests
{
    [TestFixture()]
    public class DoubleLinkedListTests
    {
        DoubleLinkedList List { get; set; }
        [SetUp]
        public void ForEachTestSetUp() {
            List = new DoubleLinkedList();
            Assert.IsNotNull(List, "Mustn't be null");
            Assert.IsTrue(List.IsEmpty,"Must be empty");
            var expectedLinkSize = 0;
            Assert.AreEqual(List.GetSize(), expectedLinkSize, "Size must be equal zero");
  
        }

        [TearDown]
        public void ForEachTestTearDown() {
            List = null;
            Assert.IsNull(List,"Must be null");
        }

        [Test()]
        public void AddFirstTest_InputIsNumber_ShouldReturnExpectedValues()
        {          
                   
            var inputValue = 7;
            List.AddFirst(inputValue);
            Assert.AreEqual(List.GetFirst(), inputValue);
            var expectedLinkSize = 0;
            Assert.AreEqual(List.GetSize(), ++expectedLinkSize);
            inputValue = 3;
            List.AddFirst(inputValue);
            Assert.AreEqual(List.GetFirst(), inputValue);
            Assert.AreEqual(List.GetSize(), ++expectedLinkSize);
            inputValue = -1;
            List.AddFirst(inputValue);
            Assert.AreEqual(List.GetFirst(), inputValue);
            Assert.AreEqual(List.GetSize(), ++expectedLinkSize);

        }

        [Test()]
        public void AddFirstTest_InputIsArray_ShouldReturnExpectedValues()
        {
            int[] inputArray = { 1, 2, 3, 3, 5 };
            var expectedLinkSize = 0;
            List.AddFirst(inputArray);
            expectedLinkSize = inputArray.Length;
            Assert.AreEqual(List.GetSize(), expectedLinkSize);            
        }

        [Test()]
        public void AddLastTest_InputIsNumber_ShouldReturnExpectedValues()
        {
            var inputValue = 7;
            List.AddLast(inputValue);
            Print(List);
            Assert.AreEqual(List.GetLast(), inputValue);
            var expectedLinkSize = 0;
            Assert.AreEqual(List.GetSize(), ++expectedLinkSize);
            inputValue = 3;
            List.AddLast(inputValue);
            Print(List);
            Assert.AreEqual(List.GetLast(), inputValue);
            Assert.AreEqual(List.GetSize(), ++expectedLinkSize);
            inputValue = -1;
            List.AddLast(inputValue);
            Print(List);
            Assert.AreEqual(List.GetLast(), inputValue);
            Assert.AreEqual(List.GetSize(), ++expectedLinkSize);

        }

        [Test()]
        public void AddAtTheBeginningTest_ShouldReturnExpectedValues()
        {
            int[] inputArray = {1, 2, 3, 4, 5};
            List.AddFirst(inputArray);
            Print(List);
            Assert.AreEqual(List.GetSize(), inputArray.Length);
            List.AddAt(0, 6);
            var expectedSize = inputArray.Length + 1;
            Print(List);
            Assert.AreEqual(List.GetSize(), expectedSize);
           
            CollectionAssert.AreEquivalent(GetArray(List), new int[] { 6,1, 2, 3, 4, 5,});
        }      

        [Test()]
        public void AddAtTheEndTest_ShouldReturnExpectedValues()
        {
            int[] inputArray = { 1, 2, 3, 4, 5 };
            List.AddLast(inputArray);
            Print(List);
            Assert.AreEqual(List.GetSize(), inputArray.Length);
            List.AddAt(inputArray.Length-1, 6);
            var expectedSize = inputArray.Length + 1;
            Assert.AreEqual(List.GetSize(), expectedSize);
            Print(List);
            
            CollectionAssert.AreEquivalent(GetArray(List), new int[] {1, 2, 3, 4,  6,5});
        }

        [Test()]
        public void AddAtTest_ShouldReturnExpectedValues()
        {
            int[] inputArray = { 1, 2, 3, 4, 5 };
            List.AddLast(inputArray);
            Print(List);
            Assert.AreEqual(List.GetSize(), inputArray.Length);
            List.AddAt(2, 6);
            var expectedSize = inputArray.Length + 1;
            Print(List);
            Assert.AreEqual(List.GetSize(), expectedSize);
          

            CollectionAssert.AreEquivalent(GetArray(List), new int[] { 1, 2, 6, 3, 4, 5 });
        }


        [Test()]
        [TestCaseSource(typeof(TestData), nameof(TestData.OutOfRangeTestCases))]
        public void AddAtTest_IndexIsNotExist_ShouldThrowException(int idx)
        {
            Assert.Throws<IndexOutOfRangeException>(() => List.AddAt(idx, 99));
        }

        [Test()]
        public void AddLastTest_InputIsArray_ShouldReturnExpectedValues()
        {
            int[] inputArray = { 1, 2, 3, 3, 5 };
          
            var expectedLinkSize = 0;        
            List.AddLast(inputArray);
            expectedLinkSize = inputArray.Length;
            Assert.AreEqual(List.GetSize(), expectedLinkSize);
        }
        public class TestData
        {
            public static IEnumerable RemoveTestCases
            {
                get
                {
                    yield return new TestCaseData(1).Returns(true).SetName("ElementIsExist_ShouldReturnTrue");                    
                    yield return new TestCaseData(-3434).Returns(false).SetName("ElementIsNotExist_ShouldReturnFalse");
                }
            }
            public static IEnumerable OutOfRangeTestCases
            {
                get { yield return new TestCaseData(-1); }


            }
        
        }

        //[Test()]
        //[TestCaseSource(typeof(TestData), nameof(TestData.RemoveTestCases))]
        //public bool RemoveTest_ShouldReturnExpectedValues(int val)
        //{
        //    int[] inputArray = {1, 2, 3, 3, 5};
        //    List.AddFirst(inputArray);
        //    Assert.AreEqual(List.GetSize(), inputArray.Length);       
        //    var answer = List.Remove(val);          
        //    return answer;
        //}

        [Test()]     
        public void RemoveAtTest_ShouldReturnExpectedValues()
        {
            var inputArray = new int[] { 1, 2, 3, 4, 5 };
            foreach (var iter in inputArray)
            {
                List.AddLast(iter);
            }
            Assert.AreEqual(List.GetSize(), inputArray.Length);
            List.RemoveAt(1);
            var expectedSize = inputArray.Length - 1;
            Assert.AreEqual(List.GetSize(), expectedSize);
            CollectionAssert.AreEqual(GetArray(List), new[] {1, 3, 4, 5 });
        }
        [Test()]
        public void RemoveAllTest_ShouldReturnExpectedValues()
        {
            int[] inputArray = { 1, 2, 3, 3,5 };             
            List.AddFirst(inputArray);
    
            Assert.AreEqual(List.GetSize(), inputArray.Length);
            List.RemoveAll(3);
            Assert.AreEqual(List.GetSize(), 3);
         
        }

        [Test()]
        public void RemoveFirstTest_ShouldReturnExpectedValues()
        {
            var inputArray = new int[]{1, 2, 3, 4, 5};
            foreach (var iter in inputArray)
            {
                List.AddLast(iter);
            }
            Assert.AreEqual(List.GetSize(), inputArray.Length);

            List.RemoveFirst();
            var expectedSize = (inputArray.Length)-1;
            Assert.AreEqual(List.GetSize(), expectedSize);

            CollectionAssert.AreEqual(GetArray(List), new[] { 2, 3, 4, 5 } );
           
        }


        [Test()]
        public void RemoveLastTest_ShouldReturnExpectedValues()
        {
            var inputArray = new int[] { 1, 2, 3, 4, 5 };
            foreach (var iter in inputArray)
            {
                List.AddLast(iter);
            }
            Assert.AreEqual(List.GetSize(), inputArray.Length);

            List.RemoveLast();
            var expectedSize = (inputArray.Length) - 1;
            Assert.AreEqual(List.GetSize(), expectedSize);

            CollectionAssert.AreEqual(GetArray(List), new[] { 1,2, 3, 4 });
        }

        [Test()]
        [TestCaseSource(typeof(TestData), nameof(TestData.OutOfRangeTestCases))]
        public void GetTest_IndexOutOfRange_ShouldThrowException(int value)
        {        
            Assert.Throws<IndexOutOfRangeException>(() => List.Get(value));
        }

        [Test()]
        [TestCase(0,8)]
        [TestCase(2,4)]
        public void GetTest_IndexIsValid_ShouldReturnExpectedValue(int value, int expectedValue)
        {
            List.AddFirst(2);
            List.AddFirst(4);
            List.AddFirst(7);
            List.AddFirst(8);

            Assert.AreEqual(expectedValue, List.Get(value));         
        }

        [Test()]
        [TestCaseSource(typeof(TestData), nameof(TestData.RemoveTestCases))]
        public bool ContainsTest_ShouldReturnExpectedValue(int value) {         
            List.AddFirst(2);
            List.AddFirst(4);
            List.AddFirst(1);
            List.AddFirst(7);
            List.AddFirst(8);
            return List.Contains(value);        
        }

       

        [Test()]
        [TestCase(0, 8)]
        [TestCase(2, 4)]
        public void SetTest_IdxIsExist_ShouldReturnExpectedValue(int idx, int value)
        {
          
            List.AddFirst(2);
            List.AddFirst(4);
            List.AddFirst(7);
            List.AddFirst(8);

            List.Set(idx, value);
            var expectedValue = List.Get(idx);
            Assert.AreEqual(expectedValue, value);
           
        }

        [Test()]
        [TestCaseSource(typeof(TestData), nameof(TestData.OutOfRangeTestCases))]
        public void SetTest_IdxIsNotExist_ShouldThrowException(int idx)
        {        
            Assert.Throws<IndexOutOfRangeException>(() => List.Set(idx, idx));
        }


        [Test()]
        [TestCase(8, 0)]
        [TestCase(2, 3)]
        public void IndexOfTest_ElementIsExist_ShouldReturnExpectedValue(int value,int expectedIdx)
        {

            List.AddFirst(2);
            List.AddFirst(4);
            List.AddFirst(7);
            List.AddFirst(8);
            Print(List);
           
            var expectedValue = List.IndexOf(value);
            Assert.AreEqual(expectedValue, expectedIdx);

        }

        [Test()]
        [TestCaseSource(typeof(TestData), nameof(TestData.OutOfRangeTestCases))]
        public void IndexOfTest_ElementIsNotExist_ShouldReturnExpe(int idx)
        {
            List.AddFirst(1);
           Assert.AreEqual(List.IndexOf(idx),-1);
        }

        [Test()]
        public void ToArrayTest_ShouldReturnExpectedValue()
        {
            var inputArray = new[] {3, 45, 67, 12, 22};
            List.AddLast(inputArray);
            var outputArray = List.ToArray();
            CollectionAssert.AreEqual(inputArray, outputArray);
        }


        [Test()]
        public void ReverseTest_ShouldReturnExpectedValue()
        {
            var inputArray = new int[] {1, -1, 4, 3, 7, 15, 3, 3, 94, -5, 8, 0};
            List.AddLast(inputArray);
            List.Reverse();

           CollectionAssert.AreEqual(GetArray(List),inputArray.Reverse());
        }


        private void Print(DoubleLinkedList list)
        {
            if (list.IsEmpty)
            {
                Console.WriteLine($@"Collection is empty");
            }

            Console.WriteLine($@"===============================");
            for (int i = 0; i < list.GetSize(); i++)
            {
                try
                {
                   Console.WriteLine($@"{List.Get(i)} on {i} position");
                }
                catch (NullReferenceException )
                {
                    Console.WriteLine($@"Null at {i} position");
                   
                }
            }
            Console.WriteLine($@"===============================");
        }

        private  int [] GetArray(DoubleLinkedList list)
        {
            var outPut = new int[list.GetSize()];
            for (var i = 0; i < list.GetSize(); i++)
            {
                try
                {
                    outPut[i] = list.Get(i);
                }
                catch (NullReferenceException)
                {
                    Console.WriteLine($@"Null at {i} position");

                }
            }          
            return outPut;
        }
    }
}
